# História do jogo
Estrela de lã   

21 ABY

Ajude-me quem puder!!!!
Eu (Cat Vader) estava bricando alegremente com o meu novo novelo de lã, até que fui dormir.
Quando acordei, vi que meu novelo desapareceu e eu amo mais que tudo brincar com ele.
Então resolvi procurar nos  corredores para encontrar minha amada bolinha; mas percebi que
o meu inimigo além de roubar, deixou armadilhas para dificultar o caminho e colocou um
agente de combate par me derrotar e ficar com  a Estrela de lã e o meu novelo.

Estou com dificuldades para derrotar meus inimigos e passar pelos  obstáculos sozinho.
Me ajuda? Por favor. Vamos enfrentar essa batalha  Juntos.

Com todo amor e ódio, Cat Vader.

## Fluxo do Jogo
O jogo acontece numa plataforma onde deve controlar o Cat Vader, aonde consegue ver
uma parte a frente para guiar o personagem.
O personagem irá passar por algumas dificuldades como enfretar  os inimigos e até mesmo 
desviar dos obstáculos para chegar no seu objetivo final que é resgatar o seu novelo de lã.
No meio do caminho conseguirá caçar algumas moedas e também vida.
As moedas servirão para comprar naves para lutas mais intensas e a vida será utilizada
Para a nave ficar mais tempo no jogo. Cada nave ficará 5 segundos no jogo.

## Mundo dos jogos
A primeira fase do jogo acontece no corredor da estrela de lã que é o mesmo corredor do filme
que tem o nome de Estrela da Morte. O Cat Vader que é o personagem principal deverá lutar 
com Birdtrooper, passar por obstáculos e também pelo chefão
 Já na segunda fase já passa na Tatooine aonde é o planeta natal do skywalker e aonde o
Ra2to2 foi descoberto. Então o Vader lutará com ele, haverá novos obstáculos e também 
lutará com o chefão. 
As fases ocorrerá em todas os planetas do mundo star wars.

## Jogabilidade

 O jogo é de aventura, onde o personagem passa por varios planetas ao passar de fase e
Também conhece novos inimigos, coleta as w-wings e as armas e/ou sabre de luz deles.

É feito pelo unity 2D, o jogo é off-line, pode ser jogado no android e computador. O estilo
Do jogo é em terceira pessoa, onde pode controlar quando pular, andar, correr e atirar.

## Experiencia do jogador
A primeira tela é onde o jogador escolhe se quer começar o jogo ou acessar o guia de ajuda para 
verificar  as teclas de atalho. 

Ao iniciar o jogo o jogador entra logo na primeira fase e quando derrotar o inimigo final, ele
passará logo para a segunda fase. As fases terão chefãos e cada inimigo terá muito mais 
Vida.

O personagem irá começar com quatro vidas e essas vidas serão renovadas a cada fase. Se o 
personagem perder as vidas irá começar a uma fase anterior para voltar para a mesma fase
Que estava antes.

## Inimigos

### Jabba the Hutt
Jabba Tiure, foi um senhor do crime Hutt e membro do grande conselho Hutt. Jabba operava
seu gigantesco mundo do crime a partir do seu palácio localizado em Tatooine.

### Yoda
Yoda, é conhecido pela enorme sabedoria, conhecedor produndo da força e habilidades em 
Combates com sabre de luz.

## Bonus
   Durante o jogo irá tem algumas moedas para o cat vader coletar e também ao combater o 
chefão conseguirá mais 30(trinta) moedas.
  Cada combate com o chefão o personagem irá ganhar também as x-wings dos inimigos e 
com as vidas coletadas poderá melhorar as naves.
